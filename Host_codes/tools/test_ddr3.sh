#!/bin/bash

addrOffsetBase=0xC00000000
transferBlock=1

transferSize=$((32*1024*1024))
transferCount=1
h2cChannels=1
c2hChannels=1

tool_path=../tools

testError=0

# Write to all enabled h2cChannels in parallel
if [ $h2cChannels -gt 0 ]; then
  # Loop over four blocks of size $transferSize and write to them (in parallel where possible)
  for ((i=0; i<transferBlock; i++))
  do
    addrOffset=$(($transferSize * $i + addrOffsetBase))
    curChannel=$(($i % $h2cChannels))
    curFile=$(($i % $h2cChannels))
    printf "Info: Writing to   h2c channel %d at address offset 0x%016x \n" $curChannel $addrOffset
    $tool_path/dma_to_device -d /dev/xdma0_h2c_${curChannel} -f ../data/datafile${curFile}_32M.bin -s $transferSize -a $addrOffset -c $transferCount &
    # If all channels have active transactions we must wait for them to complete
    if [ $(($curChannel+1)) -eq $h2cChannels ]; then
      echo "Info: Wait for current transactions to complete."
      wait
    fi
  done
fi

# Wait for the last transaction to complete.
wait

# Read from all enabled c2hChannels in parallel
if [ $c2hChannels -gt 0 ]; then
  # Loop over four blocks of size $transferSize and read from them (in parallel where possible)
  for ((i=0; i<transferBlock; i++))
  do
    addrOffset=$(($transferSize * $i + addrOffsetBase))
    curChannel=$(($i % $c2hChannels))
    #rm -f data/output_datafile${i}_4K.bin
    rm -f ../data/output_datafile${i}_32M.bin
    #echo "Info: Reading from c2h channel $curChannel at address offset "
    printf "Info: Reading from c2h channel %d at address offset 0x%016x \n" $curChannel $addrOffset
    $tool_path/dma_from_device -d /dev/xdma0_c2h_${curChannel} -f ../data/output_datafile${i}_32M.bin -s $transferSize -a $addrOffset -c $transferCount &
    # If all channels have active transactions we must wait for them to complete
    if [ $(($curChannel+1)) -eq $c2hChannels ]; then
      echo "Info: Wait for the current transactions to complete."
      wait
    fi
  done
fi

# Wait for the last transaction to complete.
wait

# Verify that the written data matches the read data if possible.
if [ $h2cChannels -eq 0 ]; then
  echo "Info: No data verification was performed because no h2c channels are enabled."
elif [ $c2hChannels -eq 0 ]; then
  echo "Info: No data verification was performed because no c2h channels are enabled."
else
  echo "Info: Checking data integrity."
  for ((i=0; i<transferBlock; i++))
  do
    curInfile=$(($i % $h2cChannels))
    cmp ../data/output_datafile${i}_32M.bin ../data/datafile${curInfile}_32M.bin -n $transferSize
    returnVal=$?
    if [ ! $returnVal == 0 ]; then
      echo "Error: The data written did not match the data that was read."
      echo "       address range:   $(($i*$transferSize)) - $((($i+1)*$transferSize))"
      echo "       write data file: data/datafile${curInfile}_32M.bin"
      echo "       read data file:  data/output_datafile${i}_32M.bin"
      testError=1
    else
      echo "Info: Data check passed for address range $(($i*$transferSize)) - $((($i+1)*$transferSize))."
    fi
  done
fi

# Exit with an error code if an error was found during testing
if [ $testError -eq 1 ]; then
  echo "Error: Test completed with Errors."
  exit 1
fi

# Report all tests passed and exit
echo "Info: All PCIe DMA memory mapped tests passed."
exit 0

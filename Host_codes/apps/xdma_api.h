#ifndef __XDMA_API_INCLUDE__
#define __XDMA_API_INCLUDE__

#define RW_MAX_SIZE	0x7ffff000

enum {
	CHAN_H2C_0,
	CHAN_H2C_1,
	CHAN_H2C_2,
	CHAN_H2C_3,
	CHAN_H2C_MAX
};

enum {
	CHAN_C2H_0,
	CHAN_C2H_1,
	CHAN_C2H_2,
	CHAN_C2H_3,
	CHAN_C2H_MAX
};
enum {
	DMA_DIR_H2C,
	DMA_DIR_C2H
};

#define DEVNODE_NAME_MAX 32

enum {
	USR_EVENT_0,
	USR_EVENT_1,
	USR_EVENT_2,
	USR_EVENT_3,
	USR_EVENT_MAX
};

enum {
	AXILITE_MASTER_0,
	AXILITE_MASTER_MAX
};

enum {
	ACCEL_0,
	ACCEL_MAX
};

/**
 * xdma_channel_open - Open PCIe DMA channel
 * @channel: DMA channel number (0 -3)
 * @dir: DMA direction (0 - host to card, 1 - card to host)
 *
 * Return: On success, retrun non-negative integer representing file
 * descriptor of the device. Otherwise, return -ve value
 */
int xdma_channel_open(int channel, int dir);

/**
 * xdma_channel_close - Close PCIe DMA channel
 * @fd: File descriptor of the device
 *
 */
void xdma_channel_close(int fd);

/**
 * xdma_h2c_transfer - DMA transfer from host to card
 * @fd: File descriptor of the device
 * @buffer: Source address (must align to 4KB boundary)
 * @size: Transfer size in byte
 * @dest_addr: Destination address to pcie device
 *
 * Return: On success, return transfered size. Otherwise, return
 * -ve value
 */
ssize_t xdma_h2c_transfer(int fd, char *buffer, uint64_t size, uint64_t dest_addr);

/**
 * xdma_c2h_transfer - DMA transfer from card to host
 * @fd: File descriptor of the device
 * @buffer: Destination address (must align to 4KB boundary)
 * @size: Transfer size in byte
 * @src_addr: Source address from pcie device
 *
 * Return: On success, return transfered size. Otherwise, return
 * -ve value
 */
ssize_t xdma_c2h_transfer(int fd, char *buffer, uint64_t size, uint64_t src_addr);

/**
 * read_to_buffer - Read data from device node
 * @fname: Any string for debug purpose
 * @fd: File descriptor of the device
 * @buffer: Destination address (must align to 4KB boundary)
 * @size: Transfer size in byte
 * @base: Source address from device
 *
 * Return: On success, return transfered size. Otherwise, return
 * -ve value
 */
ssize_t read_to_buffer(char *fname, int fd, char *buffer, uint64_t size,
			uint64_t base);

/**
 * write_from_buffer - Write data to device node
 * fname: Any string for debug purpose
 * @fd: File descriptor of the device
 * @buffer: Source address (must align to 4KB boundary)
 * @size: Transfer size in byte
 * @base: Destination address on device node
 *
 * Return: On success, return transfered size. Otherwise, return
 * -ve value
 */
ssize_t write_from_buffer(char *fname, int fd, char *buffer,
		uint64_t size, uint64_t base);

/**
 * xdma_axilite_master_open - Open axilite master device
 * @id: User control device id (should be 0)
 * Return: On success, retrun non-negative integer representing file
 * descriptor of control port. Otherwise, return -ve value
 */
int xdma_axilite_master_open(int id);

/**
 * xdma_axilite_master_unmmap - Mapping bar address to virtual space
 * @map_base: Mapped address base
 * @size: Map region size
 *
 * Return: On success, return 0. Otherwise, deadloop
 */
int xdma_axilite_master_mumap(void *map_base, size_t size);

/**
 * xdma_axilite_master_close - Close axilite master device
 * @fd: File descriptor of user control device
 *
 */
void xdma_axilite_master_close(int fd);

/**
 * xdma_axilite_master_mmap - Mapping bar address to virtual space
 * @fd: File descriptor of control port
 * @size: Map region size
 * @map_base: Mapped address base
 * @offset: Offset to file start
 *
 * Return: On success, return 0. Otherwise, deadloop
 */
int xdma_axilite_master_mmap(int fd, size_t size, void **map_base, off_t offset);

/**
 * xdma_user_event_open - Open user event device
 * @event_num: User event num (0 - 7)
  *
 * Return: On success, retrun non-negative integer representing file
 * descriptor of the device. Otherwise, return -ve value
 */
int xdma_usr_event_open(int event_num);

/**
 * xdma_usr_event_close - Close user event device
 * @fd: File descriptor of the device
 *
 */
void xdma_usr_event_close(int fd);

/**
 * xdma_usr_event_wait - Blocking wait for event
 * @fd: File descriptor of the device
 * @event: Event num
 *
 */
ssize_t xdma_usr_event_wait(int fd, uint32_t *event);

/**
 * xdma_start_accel - Start accelerator
 * @map_base: Mapped base address of axilite master
 * @id: Accelerator ID
 *
 * Return: On success, return 0. Otherwise, return -ve value
 */
int xdma_start_accel(void *map_base, int id);

/**
 * xdma_reset_accel - Reset accelerator
 * @map_base: Mapped base address of axilite master
 * @id: Accelerator ID
 *
 * Return: On success, return 0. Otherwise, return -ve value
 * Note: not thread-safe
 */
int xdma_reset_accel(void *map_base, int id);

/**
 * xdma_set_params - Pass parameters to accelerator
 * @map_base: Mapped base address of axilite master
 * @src_addr_hi: Source address of data in devcie side (high 32bit)
 * @src_addr_lo: Source address of data in devcie side (low 32bit)
 * @dst_addr_hi: Source address of data in devcie side (high 32bit)
 * @dst_addr_lo: Source address of data in devcie side (low 32bit)
 * @size: Data size in byte
 * @cmd: Command
 * @param1: Parameters
 * @param2: Parameters
 *
 * Return: Always return 0
 * Note: not thread-safe
 */
int xdma_set_params(void *map_base, uint32_t src_addr_hi, \
	uint32_t src_addr_lo, uint32_t dst_addr_hi, uint32_t dst_addr_lo, \
	uint32_t size_hi, uint32_t size_lo, uint32_t cmd, \
	uint32_t param1, uint32_t param2);

	int xdma_reg_write(void *map_base, uint32_t dst_addr, uint32_t data);
	int xdma_reg_read(void *map_base, uint32_t dst_addr, uint32_t *read_data);

/**
 * xdma_wait_accel_ready - Waiting accelerator ready
 * @map_base: Mapped base address of axilite master
 * @id: Accerlerator ID
 *
 * Return: Always return 0
 * Note: not thread-safe
 */
int xdma_wait_accel_ready(void *map_base, int id);

#endif

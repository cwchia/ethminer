/*
 * This file contains APIs to interactive with Xilinx PCIe DMA/Bridge
 * Subsystem device driver
 *
 * Copyright (c) 2019-present,  Avnet
 * All rights reserved.
 *
 * This source code is licensed under both the BSD-style license (found in the
 * LICENSE file in the root directory of this source tree) and the GPLv2 (found
 * in the COPYING file in the root directory of this source tree).
 * You may select, at your option, one of the above-listed licenses.
 */

#define _BSD_SOURCE
#define _XOPEN_SOURCE 500
#include <assert.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "xdma_api.h"

#define FATAL do { fprintf(stderr, "Error at line %d, file %s (%d) [%s]\n", __LINE__, __FILE__, errno, strerror(errno)); exit(1); } while(0)

const char h2c_devname[CHAN_H2C_MAX][DEVNODE_NAME_MAX] = {
	"/dev/xdma0_h2c_0",
	"/dev/xdma0_h2c_1",
	"/dev/xdma0_h2c_2",
	"/dev/xdma0_h2c_3",
};

const char c2h_devname[CHAN_C2H_MAX][DEVNODE_NAME_MAX] = {
	"/dev/xdma0_c2h_0",
	"/dev/xdma0_c2h_1",
	"/dev/xdma0_c2h_2",
	"/dev/xdma0_c2h_3",
};

const char usr_event_devname[USR_EVENT_MAX][DEVNODE_NAME_MAX] = {
	"/dev/xdma0_events_0",
	"/dev/xdma0_events_1",
	"/dev/xdma0_events_2",
	"/dev/xdma0_events_3",
};

const char axilite_master_devname[AXILITE_MASTER_MAX][DEVNODE_NAME_MAX] = {
	"/dev/xdma0_user",
};

#define AXI_GPIO_REG_BASE	(0x00010000UL)
#define GPIO_DATA_REG		(AXI_GPIO_REG_BASE)
#define AXI_BRAM_BASE		(0x00000000UL)
#define BRAM_START_ADDR	(AXI_BRAM_BASE)
#define ACCEL_RDY_OFS		(0)		// Accelerator ready status offset
#define ACCEL_SIG_OFS		(4)		// Accelerator ready status offset
#define AXI_REG_RD_WR_BASE		(0x00020000UL)
#define REG_RD_WR_ADDR	(AXI_REG_RD_WR_BASE)
/**
 * xdma_channel_open - Open PCIe DMA channel
 * @channel: DMA channel number (0 -3)
 * @dir: DMA direction (0 - host to card, 1 - card to host)
 *
 * Return: On success, retrun non-negative integer representing file
 * descriptor of the device. Otherwise, return -ve value
 */
int xdma_channel_open(int channel, int dir)
{
	const char *devname;

	if (dir == DMA_DIR_H2C) {
		devname = h2c_devname[channel];
	}
	else {
		devname = c2h_devname[channel];
	}
	return open(devname, O_RDWR);
}

/**
 * xdma_channel_close - Close PCIe DMA channel
 * @fd: File descriptor of the device
 *
 */
void xdma_channel_close(int fd)
{
	close(fd);
}

/**
 * write_from_buffer - Write data to device node
 * fname: Any string for debug purpose
 * @fd: File descriptor of the device
 * @buffer: Source address (must align to 4KB boundary)
 * @size: Transfer size in byte
 * @base: Destination address on device node
 *
 * Return: On success, return transfered size. Otherwise, return
 * -ve value
 */
ssize_t write_from_buffer(char *fname, int fd, char *buffer,
		uint64_t size, uint64_t base)
{
	ssize_t rc;
	uint64_t count = 0;
	char *buf = buffer;
	off_t offset = base;

	while (count < size) {
		uint64_t bytes = size - count;

		if (bytes > RW_MAX_SIZE)
			bytes = RW_MAX_SIZE;

		if (offset) {
			rc = lseek(fd, offset, SEEK_SET);
			if (rc != offset) {
				fprintf(stderr, "%s, seek off 0x%lx != 0x%lx.\n",
					fname, rc, offset);
				perror("seek file");
				return -EIO;
			}
		}

		/* write data to file from memory buffer */
		rc = write(fd, buf, bytes);
		if (rc != bytes) {
			fprintf(stderr, "%s, W off 0x%lx, 0x%lx != 0x%lx.\n",
				fname, offset, rc, bytes);
				perror("write file");
			return -EIO;
		}

		count += bytes;
		buf += bytes;
		offset += bytes;
	}

	if (count != size) {
		fprintf(stderr, "%s, R failed 0x%lx != 0x%lx.\n",
				fname, count, size);
		return -EIO;
	}
	return count;
}

/**
 * xdma_h2c_transfer - DMA transfer from host to card
 * @fd: File descriptor of the device
 * @buffer: Source address (must align to 4KB boundary)
 * @size: Transfer size in byte
 * @dest_addr: Destination address on dma device
 *
 * Return: On success, return transfered size. Otherwise, return
 * -ve value
 */
ssize_t xdma_h2c_transfer(int fd, char *buffer, uint64_t size, uint64_t dest_addr)
{
	return write_from_buffer("h2c chan", fd, buffer, size, dest_addr);
}

/**
 * read_to_buffer - Read data from device node
 * @fname: Any string for debug purpose
 * @fd: File descriptor of the device
 * @buffer: Destination address (must align to 4KB boundary)
 * @size: Transfer size in byte
 * @base: Source address from device
 *
 * Return: On success, return transfered size. Otherwise, return
 * -ve value
 */
ssize_t read_to_buffer(char *fname, int fd, char *buffer, uint64_t size,
			uint64_t base)
{
	ssize_t rc;
	uint64_t count = 0;
	char *buf = buffer;
	off_t offset = base;

	while (count < size) {
		uint64_t bytes = size - count;

		if (bytes > RW_MAX_SIZE)
			bytes = RW_MAX_SIZE;

		if (offset) {
			rc = lseek(fd, offset, SEEK_SET);
			if (rc != offset) {
				fprintf(stderr, "%s, seek off 0x%lx != 0x%lx.\n",
					fname, rc, offset);
				perror("seek file");
				return -EIO;
			}
		}

		/* read data from file into memory buffer */
		rc = read(fd, buf, bytes);
		if (rc != bytes) {
			fprintf(stderr, "%s, R off 0x%lx, 0x%lx != 0x%lx.\n",
				fname, count, rc, bytes);
				perror("read file");
			return -EIO;
		}

		count += bytes;
		buf += bytes;
		offset += bytes;
	}

	if (count != size) {
		fprintf(stderr, "%s, R failed 0x%lx != 0x%lx.\n",
				fname, count, size);
		return -EIO;
	}
	return count;
}

/**
 * xdma_c2h_transfer - DMA transfer from card to host
 * @fd: File descriptor of the device
 * @buffer: Destination address (must align to 4KB boundary)
 * @size: Transfer size in byte
 * @src_addr: Source address from dma device
 *
 * Return: On success, return transfered size. Otherwise, return
 * -ve value
 */
ssize_t xdma_c2h_transfer(int fd, char *buffer, uint64_t size, uint64_t src_addr)
{
	return read_to_buffer("c2h_chan", fd, buffer, size, src_addr);
}

/**
 * xdma_axilite_master_open - Open axilite master device
 * @id: User control device id (should be 0)
 * Return: On success, retrun non-negative integer representing file
 * descriptor of control port. Otherwise, return -ve value
 */
int xdma_axilite_master_open(int id)
{
	const char *devname;
	devname = axilite_master_devname[id];
	return open(devname, O_RDWR | O_SYNC);
}

/**
 * xdma_axilite_master_close - Close axilite master device
 * @fd: File descriptor of user control device
 *
 */
void xdma_axilite_master_close(int fd)
{
	close(fd);
}

/**
 * xdma_axilite_master_mmap - Mapping bar address to virtual space
 * @fd: File descriptor of control port
 * @size: Map region size
 * @map_base: Mapped address base
 * @offset: Offset to file start
 *
 * Return: On success, return 0. Otherwise, deadloop
 */
int xdma_axilite_master_mmap(int fd, size_t size, void **map_base, off_t offset)
{
	void *map_base_t;

	map_base_t = mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, offset);
	if (map_base_t == (void *)-1)
		FATAL;
	//printf("Memory mapped at address %p.\n", map_base_t);

	*map_base = map_base_t;
	return 0;
}

/**
 * xdma_axilite_master_unmmap - Mapping bar address to virtual space
 * @map_base: Mapped address base
 * @size: Map region size
 *
 * Return: On success, return 0. Otherwise, deadloop
 */
int xdma_axilite_master_mumap(void *map_base, size_t size)
{
	if (munmap(map_base, size) == -1)
		FATAL;
	return 0;
}

/**
 * xdma_user_event_open - Open user event device
 * @event_num: User event num (0 - 7)
  *
 * Return: On success, retrun non-negative integer representing file
 * descriptor of the device. Otherwise, return -ve value
 */
int xdma_usr_event_open(int event_num)
{
	const char *devname;
	devname = usr_event_devname[event_num];
	//printf("event devnode %s\n", devname); // testing
	return open(devname, O_RDWR | O_SYNC);
}

/**
 * xdma_usr_event_close - Close user event device
 * @fd: File descriptor of the device
 *
 */
void xdma_usr_event_close(int fd)
{
	close(fd);
}

/**
 * xdma_usr_event_wait - Blocking wait for event
 * @fd: File descriptor of the device
 * @event: Event num
 *
 */
ssize_t xdma_usr_event_wait(int fd, uint32_t *event)
{
	uint32_t value;
	ssize_t rc;

	rc = read(fd, &value, 4);
	if (rc == 4) {
		*event = value;
		//printf("%s: event num %d\n", __func__, value); // testing
		return 0;
	}

	return rc;
}

/**
 * xdma_start_accel - Start accelerator
 * @map_base: Mapped base address of axilite master
 * @id: Accelerator ID
 *
 * Return: On success, return 0. Otherwise, return -ve value
 * Note: not thread-safe
 */
int xdma_start_accel(void *map_base, int id)
{
	void *reg_addr;
	uint32_t write_val, read_val;

	reg_addr = map_base + GPIO_DATA_REG;
	//printf("%s: reg_base %p\n", __func__, reg_addr);
	read_val = *((uint32_t *) reg_addr);
	write_val = read_val | (0x00000100 << id);
	//printf("%s: read_val 0x%x write_val 0x%x\n", __func__, read_val, write_val);
	*((uint32_t *) reg_addr) = write_val;
	usleep(1000);
	*((uint32_t *) reg_addr) = read_val;

	return 0;
}

/**
 * xdma_reset_accel - Reset accelerator
 * @map_base: Mapped base address of axilite master
 * @id: Accelerator ID
 *
 * Return: On success, return 0. Otherwise, return -ve value
 * Note: not thread-safe
 */
int xdma_reset_accel(void *map_base, int id)
{
	volatile void *reg_addr;
	uint32_t write_val, read_val, rst_bit;
	volatile uint32_t *bram_addr;

	/* clear accelerator ready status */
	bram_addr = map_base + BRAM_START_ADDR + ACCEL_RDY_OFS;
	*bram_addr = 0;

	/* assert reset */
	reg_addr = map_base + GPIO_DATA_REG;
	//printf("%s: reg_base %p\n", __func__, reg_addr);
	read_val = *((uint32_t *) reg_addr);
	rst_bit = (0x00000010 << id);
	write_val = read_val | rst_bit; // out of reset
	printf("%s: read_val 0x%x write_val 0x%x\n", __func__, read_val, write_val);
	printf("%s: start\n", __func__);
	*((volatile uint32_t *) reg_addr) = write_val;
	usleep(1000);
	write_val = read_val & ~rst_bit; // assert reset
	printf("%s: read_val 0x%x write_val 0x%x\n", __func__, read_val, write_val);
	*((volatile uint32_t *) reg_addr) = write_val;
	usleep(1000);
	write_val = read_val | rst_bit; // out of reset
	printf("%s: read_val 0x%x write_val 0x%x\n", __func__, read_val, write_val);
	*((volatile uint32_t *) reg_addr) = write_val;
	printf("%s: stop\n", __func__);
	return 0;
}

/**
 * xdma_set_params - Pass parameters to accelerator
 * @map_base: Mapped base address of axilite master
 * @src_addr_hi: Source address of data in devcie side (high 32bit)
 * @src_addr_lo: Source address of data in devcie side (low 32bit)
 * @dst_addr_hi: Source address of data in devcie side (high 32bit)
 * @dst_addr_lo: Source address of data in devcie side (low 32bit)
 * @size: Data size in byte
 * @cmd: Command
 * @param1: Parameters
 * @param2: Parameters
 *
 * Return: Always return 0
 * Note: not thread-safe
 */
int xdma_set_params(void *map_base, uint32_t src_addr_hi, \
	uint32_t src_addr_lo, uint32_t dst_addr_hi, uint32_t dst_addr_lo, \
	uint32_t size_hi, uint32_t size_lo, uint32_t cmd, \
	uint32_t param1, uint32_t param2)
{

	volatile uint32_t *bram_addr;

	bram_addr = map_base + BRAM_START_ADDR + ACCEL_SIG_OFS;
	//printf("%s: bram_addr %p\n", __func__, bram_addr);
	*bram_addr++ = 0xdeadbeef;
	*bram_addr++ = src_addr_hi;
	*bram_addr++ = src_addr_lo;
	*bram_addr++ = dst_addr_hi;
	*bram_addr++ = dst_addr_lo;
	*bram_addr++ = size_hi;
	*bram_addr++ = size_lo;
	*bram_addr++ = cmd;
	*bram_addr++ = param1;
	*bram_addr++ = param2;

	return 0;
}

int xdma_reg_write(void *map_base, uint32_t dst_addr, uint32_t data)
{

	volatile uint32_t *addr;

	addr = map_base + REG_RD_WR_ADDR + dst_addr;
	*addr = data;

	printf("%s: ADDR %p, DATA: %x\n", __func__, addr, data);

	return 0;
}

int xdma_reg_read(void *map_base, uint32_t dst_addr, uint32_t *read_data)
{

	volatile uint32_t *addr;

	addr = map_base + REG_RD_WR_ADDR + dst_addr;
	read_data = *addr;

	printf("%s: ADDR %p, DATA: %x\n", __func__, addr, read_data);

	return 0;
}
/**
 * xdma_wait_accel_ready - Waiting accelerator ready
 * @map_base: Mapped base address of axilite master
 * @id: Accerlerator ID
 *
 * Return: Always return 0
 * Note: not thread-safe
 */
int xdma_wait_accel_ready(void *map_base, int id)
{
	volatile uint32_t *bram_addr;

	bram_addr = map_base + BRAM_START_ADDR + ACCEL_RDY_OFS;
	printf("%s: bram_addr %p\n", __func__, bram_addr);
	printf("%s: start\n", __func__);
	while (*bram_addr != 0xa5a5a5a5);
	printf("%s: stop\n", __func__);

	return 0;
}



////

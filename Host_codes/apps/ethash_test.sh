#!/bin/bash

# with fixed dram value (skip access to DDR)
sudo ./bin/accel_test -v 1 -r 1
# read back result
sudo ./bin/accel_test -v 1 -r 2

# with access to DDR
sudo ./bin/accel_test -v 1 -r 3
# read back the result, should be same as previous
sudo ./bin/accel_test -v 1 -r 2
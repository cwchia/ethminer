#define _BSD_SOURCE
#define _XOPEN_SOURCE 500
#include <assert.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "xdma_api.h"

#define RESET_ACCEL
int verbose = 0;

static struct option const long_opts[] = {
	{"reg_access", required_argument, NULL, 'r'},
	{"address_h2c", required_argument, NULL, 'a'},
	{"address_c2h", required_argument, NULL, 'b'},
	{"size", required_argument, NULL, 's'},
	{"offset", required_argument, NULL, 'o'},
	{"chan", required_argument, NULL, 'c'},
	{"data infile", required_argument, NULL, 'f'},
	{"help", no_argument, NULL, 'h'},
	{"verbose", no_argument, NULL, 'v'},
	{0, 0, 0, 0}
};

#define SIZE_DEFAULT 						(32)
#define CHAN_DEFAULT 						(CHAN_H2C_0)
//#define ACCEL_EVENT 						(USR_EVENT_2)
#define ACCEL_EVENT 						(USR_EVENT_0)
#define ACCEL_DEFAULT						(ACCEL_0)
#define AXILITE_MASTER_MAP_SIZE	(256 * 1024UL)

static int test_accel(int dma_chan, uint64_t addr_h2c, uint64_t addr_c2h,
			uint64_t size, uint64_t offset, char *infname, int reg_access);
//static int test_reg(int reg_access, uint32_t addr_h2c, uint32_t data_h2c);

static void usage(const char *name)
{
	int i = 0;

	fprintf(stdout, "%s\n\n", name);
	fprintf(stdout, "usage: %s [OPTIONS]\n\n", name);
	fprintf(stdout,
		"Write via SGDMA, optionally read input from a file.\n\n");

	fprintf(stdout, "  -%c (--%s) the start address on the AXI bus (H2C)\n",
		long_opts[i].val, long_opts[i].name);
	i++;
	fprintf(stdout, "  -%c (--%s) the start address on the AXI bus (C2H)\n",
		long_opts[i].val, long_opts[i].name);
	i++;
	fprintf(stdout,
		"  -%c (--%s) size of a single transfer in bytes, default %d,\n",
		long_opts[i].val, long_opts[i].name, SIZE_DEFAULT);
	i++;
	fprintf(stdout, "  -%c (--%s) page offset of transfer\n",
		long_opts[i].val, long_opts[i].name);
	i++;
	fprintf(stdout, "  -%c (--%s) DMA Channel Num, default %d\n",
		long_opts[i].val, long_opts[i].name, CHAN_DEFAULT);
	i++;
	fprintf(stdout, "  -%c (--%s) filename to read the data from.\n",
		long_opts[i].val, long_opts[i].name);
	i++;
	fprintf(stdout, "  -%c (--%s) print usage help and exit\n",
		long_opts[i].val, long_opts[i].name);
	i++;
	fprintf(stdout, "  -%c (--%s) verbose output\n",
		long_opts[i].val, long_opts[i].name);
	i++;

}

uint64_t getopt_integer(char *optarg)
{
	int rc;
	uint64_t value;

	rc = sscanf(optarg, "0x%lx", &value);
	if (rc <= 0)
		rc = sscanf(optarg, "%lu", &value);
	//printf("sscanf() = %d, value = 0x%lx\n", rc, value);

	return value;
}

int main(int argc, char *argv[])
{
	int cmd_opt;
	uint64_t address_h2c = 0;
	uint64_t address_c2h = 0;
	uint64_t size = SIZE_DEFAULT;
	uint64_t offset = 0;
	uint64_t dma_chan = CHAN_DEFAULT;
	char *infname = NULL;
	int reg_access;
	uint32_t reg_addr = 0;
	uint32_t reg_data = 0;
	

	while ((cmd_opt =
		getopt_long(argc, argv, "vhc:f:a:b:s:o:r:", long_opts,
			    NULL)) != -1) {
		switch (cmd_opt) {
		case 0:
			/* long option */
			break;
		case 'a':
			/* RAM address on the AXI bus in bytes (H2C) */
			address_h2c = getopt_integer(optarg);
			break;
		case 'b':
			/* RAM address on the AXI bus in bytes (C2H) */
			address_c2h = getopt_integer(optarg);
			break;
		case 's':
			/* size in bytes */
			size = getopt_integer(optarg);
			break;
		case 'o':
			offset = getopt_integer(optarg) & 4095;
			break;
			/* count */
		case 'c':
			dma_chan = getopt_integer(optarg);
			break;
			/* count */
		case 'f':
			infname = strdup(optarg);
			break;
		case 'r':
			/* Register access - 1 for write, 2 for read */
			reg_access = getopt_integer(optarg);
			break;
			/* print usage help and exit */
		case 'v':
			verbose = 1;
			break;
		case 'h':
		default:
			usage(argv[0]);
			exit(0);
			break;
		}
	}

	if (verbose) {
		printf("\n");
		printf("============== Demo Description ================\n");
		printf(" This example code demonstrate how to interact  \n");
		printf(" with Xilinx PCIe DMA Engine and the accelerator\n");
		printf(" behine. That includes:                         \n");
		printf(" - DMA from host to Card memory                 \n");
		printf(" - DMA from card to host memory                 \n");
		printf(" - Capture events from accelerator              \n");
		printf(" - Fire events to accelerator                   \n");
		printf("============== Input Parameters ================\n");
		printf("Input parameters\n");
		printf(" address_h2c  : 0x%lx\n", address_h2c);
		printf(" address_c2h  : 0x%lx\n", address_c2h);
		printf(" size         : 0x%lx\n", size);
		printf(" offset       : 0x%lx\n", offset);
		printf(" dma_chan     : 0x%lu\n", dma_chan);
		printf(" reg_access   : 0x%lu\n", reg_access);
		printf(" (0 = dma access, 1 = reg write, 2 = reg read)\n");
	}


		return test_accel(dma_chan, address_h2c, address_c2h, size, offset, infname, reg_access);

	//return test_reg(reg_access, reg_addr, reg_data);
}

/*static int test_reg(int ops, uint32_t addr_h2c, uint32_t data_h2c){
	ssize_t rc;
	void *map_base = NULL;
	size_t map_size = AXILITE_MASTER_MAP_SIZE;
	int axilite_master_fd = -1;

	axilite_master_fd = xdma_axilite_master_open(0);
	if (axilite_master_fd < 0) {
		fprintf(stderr, "unable to open AXILITE_MASTER_%d, %d.\n",
			0, axilite_master_fd);
		perror("open device");
		rc = -EINVAL;
		goto out;
	}

	xdma_axilite_master_mmap(axilite_master_fd, map_size, &map_base, 0);

	//xdma_reg_write(map_base, 0x00000000, 0x10102020);
	printf("mapbase %x \n\n", map_base);

	return 0;

	out:
		if (map_base)
			xdma_axilite_master_mumap(map_base, map_size);
		if (axilite_master_fd != -1)
			xdma_axilite_master_close(-1);
}*/



static int test_accel(int dma_chan, uint64_t addr_h2c, uint64_t addr_c2h,
			uint64_t size, uint64_t offset, char *infname, int reg_access)
{
	ssize_t rc;
	char *buffer_h2c = NULL;
	char *buffer_c2h = NULL;
	char *allocated_h2c = NULL;
	char *allocated_c2h = NULL;
	int infile_fd = -1;
	int dmah2c_fd = -1;
	int dmac2h_fd = -1;
	int accel_evt_fd = -1;
	uint32_t accel_evt;
	int axilite_master_fd = -1;
	size_t map_size = AXILITE_MASTER_MAP_SIZE;
	void *map_base = NULL;
	uint32_t *ptr1, *ptr2;
	uint32_t seed;
	uint32_t data1, data2;
	int err = 0;
	//int i;
	uint64_t i;
	uint32_t reg_read_data;
	uint64_t target = 2944235136; //3388997504;
	uint64_t target_offset = 16; //3388997504;
	/* Open DMA channel from host to card */
	dmah2c_fd = xdma_channel_open(dma_chan, DMA_DIR_H2C);
	if (dmah2c_fd < 0) {
		fprintf(stderr, "unable to open CHAN_H2C_%d, %d.\n",
			dma_chan, dmah2c_fd);
		perror("open device");
		rc = -EINVAL;
		goto out;
	}

	/* Open DMA channel from card to host */
	dmac2h_fd = xdma_channel_open(dma_chan, DMA_DIR_C2H);
	if (dmac2h_fd < 0) {
		fprintf(stderr, "unable to open CHAN_C2H_%d, %d.\n",
			dma_chan, dmac2h_fd);
		perror("open device");
		rc = -EINVAL;
		goto out;
	}

	/* Open user event device for accelerator */
	accel_evt_fd = xdma_usr_event_open(ACCEL_EVENT);
	if (accel_evt_fd < 0) {
		fprintf(stderr, "unable to open USR_EVENT_%d, %d.\n",
			ACCEL_EVENT, accel_evt_fd);
		perror("open device");
		rc = -EINVAL;
		goto out;
	}

	/* Open axilite master device */
	axilite_master_fd = xdma_axilite_master_open(0);
	if (axilite_master_fd < 0) {
		fprintf(stderr, "unable to open AXILITE_MASTER_%d, %d.\n",
			0, axilite_master_fd);
		perror("open device");
		rc = -EINVAL;
		goto out;
	}

	xdma_axilite_master_mmap(axilite_master_fd, map_size, &map_base, 0);
	//printf("mapbase %x \n\n", map_base);
	/* Open file for test data */
	if (infname) {
		infile_fd = open(infname, O_RDONLY);
		if (infile_fd < 0) {
			fprintf(stderr, "unable to open input file %s, %d.\n",
				infname, infile_fd);
			perror("open input file");
			rc = -EINVAL;
			goto out;
		}
	}



if (!reg_access){
	/* Allocate transfer buffers */
	/* h2c */
	posix_memalign((void **)&allocated_h2c, 4096 /*alignment */ , size + 4096);
	if (!allocated_h2c) {
		fprintf(stderr, "OOM %lu.\n", size + 4096);
		rc = -ENOMEM;
		goto out;
	}
	buffer_h2c = allocated_h2c + offset;
	if (verbose)
		fprintf(stdout, " host buffer  : %p (0x%lx)\n",
			buffer_h2c, size + 4096);

	if (infile_fd >= 0) {
		rc = read_to_buffer(infname, infile_fd, buffer_h2c, size, 0);
		if (rc < 0)
			goto out;
	}

	/* c2h */
	posix_memalign((void **)&allocated_c2h, 4096 /*alignment */ , size + 4096);
	if (!allocated_c2h) {
		fprintf(stderr, "OOM %lu.\n", size + 4096);
		rc = -ENOMEM;
		goto out;
	}
	buffer_c2h = allocated_c2h + offset;
	if (verbose)
		fprintf(stdout, " host buffer  : %p (0x%lx)\n",
			buffer_c2h, size + 4096);

	memset(buffer_c2h, 0, size);

	printf("================= Start Test ===================\n");

	/* Pass parameters to accelerator */
	//srand(time(NULL));
	//seed = (uint32_t)rand();
	//xdma_set_params(map_base, (uint32_t)(addr_h2c >> 32), \
		(uint32_t)(addr_h2c), (uint32_t)(addr_c2h >> 32), (uint32_t)(addr_c2h), \
		(uint32_t)(size >> 32), (uint32_t)(size), 0, seed, 0);

	/* Start H2C transfer */
	if (verbose)
		fprintf(stdout, ">> Transfer data from host to accelerator_%d\n\n", ACCEL_DEFAULT);
	rc = xdma_h2c_transfer(dmah2c_fd, buffer_h2c, size, addr_h2c);
	if (rc < 0)
		goto out;

	/* Start accelerator */
	/*if (verbose)
		fprintf(stdout, ">> Start accelerator_%d\n\n", ACCEL_DEFAULT);*/
	#ifdef RESET_ACCEL
	//xdma_reset_accel(map_base, ACCEL_DEFAULT);
	#endif

	/*usleep(5000);
	xdma_wait_accel_ready(map_base, ACCEL_DEFAULT);
	xdma_start_accel(map_base, ACCEL_DEFAULT);*/

	/* Waiting event */
	/*if (verbose)
		fprintf(stdout, ">> Waiting event from accelerator_%d\n\n", ACCEL_DEFAULT);
	rc = xdma_usr_event_wait(accel_evt_fd, &accel_evt);
	if (rc == 0) {
		if (verbose)
			fprintf(stdout, ">> Got usr_event (0x%x)\n\n", accel_evt);
	}
	else {
		perror("Wait event error");
	}*/

	/* Start C2H transfer */
	if (verbose)
		fprintf(stdout, ">> Transfer data from accelerator_%d to host\n\n", ACCEL_DEFAULT);
	rc = xdma_c2h_transfer(dmac2h_fd, buffer_c2h, size, addr_c2h);
	if (rc < 0)
		goto out;

	/* Verify data */
	#if 0
	ptr1 = (uint32_t *)buffer_h2c;
	ptr2 = (uint32_t *)buffer_c2h;
	for (i=0; i<8; i++) {
		printf("0x%x ", *ptr1++);
	}
	printf("\n");
	for (i=0; i<8; i++) {
		printf("0x%x ", *ptr2++);
	}
	printf("\n");
	#endif

	ptr1 = (uint32_t *)buffer_h2c;
	ptr2 = (uint32_t *)buffer_c2h;
	for (i=0; i<size/4; i++) {
		data1 = *ptr1;
		data2 = *ptr2;
		
		if (data2 != data1 /*+ seed*/) {
			printf("!!!! Data mismatch 0x%x (%p) 0x%x (%p)\n\n", data1, ptr1, data2, ptr2);
			err = 1;
			break;
		}
		//if ((i >= (1*16)) & (i < (2 * 16))) printf ("i[%x] %x \n", i, data2); 
		//if (i = size) printf ("i[%lx] %x \n", i, data2); 

		//if (i = (target)) printf ("i[%lx] %x \n", i, data2); 
		//if (i = (target+target_offset)) printf ("i[%lx] %x \n", i, data2); 

		//if ((i >= (target)) & (i < (target+target_offset))) printf ("i[%lx] %x \n", i, data2); 
		/*if (i >= (target)){
			printf ("i[%lx] %x \n", i, data2); 
			//if (i < (target+target_offset))printf ("i[%lx] %x \n", i, data2); 
		}*/
		
		ptr1++;
		ptr2++;
	}

	if (verbose) {
		if (!err)
			fprintf(stdout, ">> Verification pass!!\n\n");
		printf("================ Process end ===================\n\n\n");
	}

#if 0
	/* Dirty hack: Clear pending events */
	for (i=0; i<3; i++) {
		if (verbose)
			fprintf(stdout, "Waiting accelerator event ...\n");
		rc = xdma_usr_event_wait(accel_evt_fd, &accel_evt);
		if (rc == 0) {
			if (verbose)
				fprintf(stdout, "usr_event 0x%x\n", accel_evt);
		}
		else {
			perror("Wait event error");
		}
	}
#endif
}

else if (reg_access >= 6) {
}
else if (reg_access >= 5) {
	
	fprintf(stdout, ">> clear clock_cnt & outhash_cnt!\n\n");
	xdma_reg_write(map_base, 0x00000030, 0x00100003);//reset deassert
	fprintf(stdout, ">> start  clock_cnt & outhash_cnt!\n\n");
	xdma_reg_write(map_base, 0x00000030, 0x00000003);//reset deassert

	reg_access = 0;
}
else if (reg_access >= 4) {
	printf("================= DRAM Backdoor READ ===================\n");

	xdma_reg_write(map_base, 0x00000030, 0x00000000);//reset
	xdma_reg_write(map_base, 0x00000030, 0x00020000);//enable DRAM backdoor read
	xdma_reg_write(map_base, 0x0000002C, addr_h2c);  // DRAM backdoor addr
	xdma_reg_write(map_base, 0x00000030, 0x00020001);//reset deassert
	xdma_reg_write(map_base, 0x00000030, 0x00020003);//start
	fprintf(stdout, ">> start debug!\n\n");
	usleep(5000);

	//fprintf(stdout, ">> Read from registers!\n\n");
	//printf("debug/start/reset bit=\n");
	xdma_reg_read(map_base, 0x00000030, &reg_read_data);
	printf("\naddress=\n");
	xdma_reg_read(map_base, 0x0000002c, &reg_read_data);
	printf("\ndram0 (lower 32 byte)=\n");
	xdma_reg_read(map_base, 0x00000040, &reg_read_data);
	xdma_reg_read(map_base, 0x00000044, &reg_read_data);
	xdma_reg_read(map_base, 0x00000048, &reg_read_data);
	xdma_reg_read(map_base, 0x0000004C, &reg_read_data);
	xdma_reg_read(map_base, 0x00000050, &reg_read_data);
	xdma_reg_read(map_base, 0x00000054, &reg_read_data);
	xdma_reg_read(map_base, 0x00000058, &reg_read_data);
	xdma_reg_read(map_base, 0x0000005C, &reg_read_data);
	printf("\ndram0 (upper 32 byte)=\n");
	xdma_reg_read(map_base, 0x000000A0, &reg_read_data);
	xdma_reg_read(map_base, 0x000000A4, &reg_read_data);
	xdma_reg_read(map_base, 0x000000A8, &reg_read_data);
	xdma_reg_read(map_base, 0x000000AC, &reg_read_data);
	xdma_reg_read(map_base, 0x000000B0, &reg_read_data);
	xdma_reg_read(map_base, 0x000000B4, &reg_read_data);
	xdma_reg_read(map_base, 0x000000B8, &reg_read_data);
	xdma_reg_read(map_base, 0x000000BC, &reg_read_data);
 
	xdma_reg_write(map_base, 0x00000030, 0x00060003);//start
	//fprintf(stdout, ">> Read from registers!\n\n");
	//printf("debug/start/reset bit=\n");
	//xdma_reg_read(map_base, 0x00000030, &reg_read_data);
	printf("\ndram1 (lower 32 byte)=\n");
	xdma_reg_read(map_base, 0x00000040, &reg_read_data);
	xdma_reg_read(map_base, 0x00000044, &reg_read_data);
	xdma_reg_read(map_base, 0x00000048, &reg_read_data);
	xdma_reg_read(map_base, 0x0000004C, &reg_read_data);
	xdma_reg_read(map_base, 0x00000050, &reg_read_data);
	xdma_reg_read(map_base, 0x00000054, &reg_read_data);
	xdma_reg_read(map_base, 0x00000058, &reg_read_data);
	xdma_reg_read(map_base, 0x0000005C, &reg_read_data);
	printf("\ndram1 (upper 32 byte)=\n");
	xdma_reg_read(map_base, 0x000000A0, &reg_read_data);
	xdma_reg_read(map_base, 0x000000A4, &reg_read_data);
	xdma_reg_read(map_base, 0x000000A8, &reg_read_data);
	xdma_reg_read(map_base, 0x000000AC, &reg_read_data);
	xdma_reg_read(map_base, 0x000000B0, &reg_read_data);
	xdma_reg_read(map_base, 0x000000B4, &reg_read_data);
	xdma_reg_read(map_base, 0x000000B8, &reg_read_data);
	xdma_reg_read(map_base, 0x000000BC, &reg_read_data);
 


	printf("================ Process end ===================\n\n\n");
	reg_access = 0;
}
else if (reg_access >= 3) {
	printf("================= Start Test ===================\n");

	//xdma_reg_write(map_base, 0x00000030, 0x00010000);
	//xdma_reg_write(map_base, 0x00000030, 0x80010000);

/*
256'hdee1879a1e58d12c_021d68ac7a622ee1_f030e3c9320df9dc_858ebf4ba353f77e;

dee1879a
1e58d12c
021d68ac
7a622ee1
f030e3c9
320df9dc
858ebf4b
a353f77e*/

	xdma_reg_write(map_base, 0x00000030, 0x00000000);//turn on debug

	xdma_reg_write(map_base, 0x00000000, 0xa353f77e);//inhash
	xdma_reg_write(map_base, 0x00000004, 0x858ebf4b);
	xdma_reg_write(map_base, 0x00000008, 0x320df9dc);
	xdma_reg_write(map_base, 0x0000000C, 0xf030e3c9);
	xdma_reg_write(map_base, 0x00000010, 0x7a622ee1);
	xdma_reg_write(map_base, 0x00000014, 0x021d68ac);
	xdma_reg_write(map_base, 0x00000018, 0x1e58d12c);
	xdma_reg_write(map_base, 0x0000001C, 0xdee1879a);

	xdma_reg_write(map_base, 0x00000020, 0x35F59915);//INNONCE [LSB]
	xdma_reg_write(map_base, 0x00000024, 0x9C5E48E9);//INNONCE [MSB]
	xdma_reg_write(map_base, 0x00000028, 0x0000510F);//FACTOR / num of zero
	xdma_reg_write(map_base, 0x0000002C, 0x0193FFFF);// num page

	//xdma_reg_write(map_base, 0x00000034, 0x35F69916);//STOPNONCE [LSB]
	xdma_reg_write(map_base, 0x00000034, 0xF5F69916);//STOPNONCE [LSB]
	xdma_reg_write(map_base, 0x00000038, 0x9C5E48E9);//STOPNONCE [MSB]
	xdma_reg_write(map_base, 0x00000080, 0xFFFFFFFF);//COMPARE   [LSB]
	xdma_reg_write(map_base, 0x00000084, 0xFFFFFFFF);//COMPARE   [MSB]

	//xdma_reg_write(map_base, 0x00000030, 0x00010000);//turn on debug
	xdma_reg_write(map_base, 0x00000030, 0x00000001);//reset deassert
	xdma_reg_write(map_base, 0x00000030, 0x00000003);//start
	fprintf(stdout, ">> start debug!\n\n");



	//usleep(5000);
	//fprintf(stdout, ">> set start to 0\n\n");
	//xdma_reg_write(map_base, 0x00000030, 0x00000001);//reset deassert

	//xdma_reg_write(map_base, 0x00000030, 0x00010000);//turn on debug

  /*
	if (verbose)
		fprintf(stdout, ">> Waiting event from accelerator_%d\n\n", ACCEL_DEFAULT);
	rc = xdma_usr_event_wait(accel_evt_fd, &accel_evt);
	if (rc == 0) {
		if (verbose)
			fprintf(stdout, ">> Got usr_event (0x%x)\n\n", accel_evt);
	}
	else {
		perror("Wait event error");
	}
  */

	fprintf(stdout, ">> Write to registers!\n\n");

	/*xdma_reg_write(map_base, 0x00000000, 0xAAAAFFFF);
	xdma_reg_write(map_base, 0x00000004, 0xBBBBCCCC);
	xdma_reg_write(map_base, 0x00000008, 0xDDDDEEEE);
	xdma_reg_write(map_base, 0x0000000b, 0xFFFFAAAA);*/

	/*xdma_reg_write(map_base, 0x00000000, 0x00000000);
	xdma_reg_write(map_base, 0x00000004, 0x00000000);
	xdma_reg_write(map_base, 0x00000008, 0x00000000);
	xdma_reg_write(map_base, 0x0000000b, 0x00000000);*/


	printf("================ Process end ===================\n\n\n");
	reg_access = 0;
}
else if (reg_access < 2) {
	printf("================= Start Test ===================\n");

	//xdma_reg_write(map_base, 0x00000030, 0x00010000);
	//xdma_reg_write(map_base, 0x00000030, 0x80010000);

/*
256'hdee1879a1e58d12c_021d68ac7a622ee1_f030e3c9320df9dc_858ebf4ba353f77e;

dee1879a
1e58d12c
021d68ac
7a622ee1
f030e3c9
320df9dc
858ebf4b
a353f77e*/

	xdma_reg_write(map_base, 0x00000030, 0x00000000);//turn on debug

	xdma_reg_write(map_base, 0x00000000, 0xa353f77e);//inhash
	xdma_reg_write(map_base, 0x00000004, 0x858ebf4b);
	xdma_reg_write(map_base, 0x00000008, 0x320df9dc);
	xdma_reg_write(map_base, 0x0000000C, 0xf030e3c9);
	xdma_reg_write(map_base, 0x00000010, 0x7a622ee1);
	xdma_reg_write(map_base, 0x00000014, 0x021d68ac);
	xdma_reg_write(map_base, 0x00000018, 0x1e58d12c);
	xdma_reg_write(map_base, 0x0000001C, 0xdee1879a);

	xdma_reg_write(map_base, 0x00000020, 0x35F59915);//INNONCE [LSB]
	xdma_reg_write(map_base, 0x00000024, 0x9C5E48E9);//INNONCE [MSB]
	xdma_reg_write(map_base, 0x00000028, 0x0000510F);//FACTOR / num of zero
	xdma_reg_write(map_base, 0x0000002C, 0x0193FFFF);// num page

	xdma_reg_write(map_base, 0x00000034, 0xf5F59916);//STOPNONCE [LSB]
	xdma_reg_write(map_base, 0x00000038, 0x9C5E48E9);//STOPNONCE [MSB]

	xdma_reg_write(map_base, 0x00000030, 0x00010000);//turn on debug
	xdma_reg_write(map_base, 0x00000030, 0x00010001);//reset deassert
	xdma_reg_write(map_base, 0x00000030, 0x00010003);//start
	fprintf(stdout, ">> start debug!\n\n");

  /*
	if (verbose)
		fprintf(stdout, ">> Waiting event from accelerator_%d\n\n", ACCEL_DEFAULT);
	rc = xdma_usr_event_wait(accel_evt_fd, &accel_evt);
	if (rc == 0) {
		if (verbose)
			fprintf(stdout, ">> Got usr_event (0x%x)\n\n", accel_evt);
	}
	else {
		perror("Wait event error");
	}
  */

	fprintf(stdout, ">> Write to registers!\n\n");

	/*xdma_reg_write(map_base, 0x00000000, 0xAAAAFFFF);
	xdma_reg_write(map_base, 0x00000004, 0xBBBBCCCC);
	xdma_reg_write(map_base, 0x00000008, 0xDDDDEEEE);
	xdma_reg_write(map_base, 0x0000000b, 0xFFFFAAAA);*/

	/*xdma_reg_write(map_base, 0x00000000, 0x00000000);
	xdma_reg_write(map_base, 0x00000004, 0x00000000);
	xdma_reg_write(map_base, 0x00000008, 0x00000000);
	xdma_reg_write(map_base, 0x0000000b, 0x00000000);*/


	printf("================ Process end ===================\n\n\n");
	reg_access = 0;
}
else if (reg_access >= 2) {
	printf("================= Start Test ===================\n");

	fprintf(stdout, ">> Read from registers!\n\n");
	printf("debug/start/reset bit=\n");
	xdma_reg_read(map_base, 0x00000030, &reg_read_data);
	printf("\nouthash=\n");
	xdma_reg_read(map_base, 0x00000040, &reg_read_data);
	xdma_reg_read(map_base, 0x00000044, &reg_read_data);
	xdma_reg_read(map_base, 0x00000048, &reg_read_data);
	xdma_reg_read(map_base, 0x0000004C, &reg_read_data);
	xdma_reg_read(map_base, 0x00000050, &reg_read_data);
	xdma_reg_read(map_base, 0x00000054, &reg_read_data);
	xdma_reg_read(map_base, 0x00000058, &reg_read_data);
	xdma_reg_read(map_base, 0x0000005C, &reg_read_data);
	printf("\nouthash2=\n");
	xdma_reg_read(map_base, 0x000000A0, &reg_read_data);
	xdma_reg_read(map_base, 0x000000A4, &reg_read_data);
	xdma_reg_read(map_base, 0x000000A8, &reg_read_data);
	xdma_reg_read(map_base, 0x000000AC, &reg_read_data);
	xdma_reg_read(map_base, 0x000000B0, &reg_read_data);
	xdma_reg_read(map_base, 0x000000B4, &reg_read_data);
	xdma_reg_read(map_base, 0x000000B8, &reg_read_data);
	xdma_reg_read(map_base, 0x000000BC, &reg_read_data);
	printf("\noutmix=\n");
	xdma_reg_read(map_base, 0x000000C0, &reg_read_data);
	xdma_reg_read(map_base, 0x000000C4, &reg_read_data);
	xdma_reg_read(map_base, 0x000000C8, &reg_read_data);
	xdma_reg_read(map_base, 0x000000CC, &reg_read_data);
	xdma_reg_read(map_base, 0x000000D0, &reg_read_data);
	xdma_reg_read(map_base, 0x000000D4, &reg_read_data);
	xdma_reg_read(map_base, 0x000000D8, &reg_read_data);
	xdma_reg_read(map_base, 0x000000DC, &reg_read_data);
	printf("\noutnonce=\n");
	xdma_reg_read(map_base, 0x00000060, &reg_read_data);
	xdma_reg_read(map_base, 0x00000064, &reg_read_data);

	printf("\ninterrupt=\n");
	xdma_reg_read(map_base, 0x00000068, &reg_read_data);

	printf("\nouthash_cnt=\n");
	xdma_reg_read(map_base, 0x0000006C, &reg_read_data);
	printf("clock_cnt=\n");
	xdma_reg_read(map_base, 0x00000070, &reg_read_data);

	// hashrate = (outhash_cnt / clock_cnt ) x clk_freq


	printf("compare=\n");
	xdma_reg_read(map_base, 0x00000080, &reg_read_data);
	xdma_reg_read(map_base, 0x00000084, &reg_read_data);
	
	printf("================ Process end ===================\n\n\n");

reg_access = 0;
}
	rc = 0;

out:
	if (dmah2c_fd != -1)
		xdma_channel_close(dmah2c_fd);
	if (dmac2h_fd != -1)
		xdma_channel_close(dmac2h_fd);
	if (accel_evt_fd != -1)
		xdma_usr_event_close(accel_evt_fd);
	if (map_base)
		xdma_axilite_master_mumap(map_base, map_size);
	if (axilite_master_fd != -1)
		xdma_axilite_master_close(accel_evt_fd);
	if (infile_fd >= 0)
		close(infile_fd);
	if (allocated_h2c)
		free(allocated_h2c);
	if (allocated_c2h)
		free(allocated_c2h);

	return rc;

}

// sudo lspci -d 10ee:903f -vvxxx
// sudo ./reg_rw /dev/xdma0_user 0x0000 w
// sudo ./reg_rw /dev/xdma0_user 0x0000 w 0x00000001
// sudo ./bin/accel_test -f data/datafile0_4K.bin -s 4096 -a 0x400000000 -b 0x410000000 -c 0 -v 1





// end
